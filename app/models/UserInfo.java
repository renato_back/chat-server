package models;

import java.net.URI;
import java.util.UUID;

/**
 * Created by renatopb on 05/02/16.
 */
public class UserInfo {

    private String name;
    private String email;
    private URI picture;
    private String sub;

    public static UserInfo create(String name, String email, URI picture, String sub) {
        UserInfo result = new UserInfo();
        result.name = name;
        result.email = email;
        result.picture = picture;
        result.sub = sub;
        return result;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public URI getPicture() {
        return picture;
    }

    public String getSub() {
        return sub;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", picture=" + picture +
                ", sub='" + sub + '\'' +
                '}';
    }
}
