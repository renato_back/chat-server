package controllers;

import actors.FrontDesk;
import actors.Replicant;
import actors.Reply;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Payload;
import models.UserInfo;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class Chat extends Controller {

    public static Result login() {
        Optional<Reply> reply = FrontDesk.login(Json.fromJson(request().body().asJson(), Payload.class));
        if (reply.isPresent()) {
            return ok(Json.toJson(reply.get()));
        }

        return internalServerError("Unable to login.");
    }

    public static Result logout(String token) {
        Optional<Reply> reply = FrontDesk.logout(token);
        if (reply.isPresent()) {
            return ok();
        }

        return internalServerError("Unable to process logout.");
    }

    private static void readBody() {
        JsonNode json = request().body().asJson();
        if (json == null) {
            return;
        }

        Logger.debug("Login: %s", json.toString());
    }

    public static WebSocket<JsonNode> subscribe() {
        return new WebSocket<JsonNode>() {
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) {
                AtomicBoolean waitingFirstMessage = new AtomicBoolean(true);

                in.onMessage(jsonNode -> {
                    if (!waitingFirstMessage.get()) {
                        return;
                    }

                    String userToken = jsonNode.get("token").asText();
                    Optional<Reply> userInfoReply = FrontDesk.userInfo(userToken);
                    if (!userInfoReply.isPresent()) {
                        reject(unauthorized());
                        return;
                    }

                    out.write(Json.toJson(userInfoReply.get()));
                    waitingFirstMessage.set(false);

                    try {
                        Replicant.subscribe(userInfoReply.get().getUserInfo(), in, out);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        };
    }
}
