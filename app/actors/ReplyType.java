package actors;

/**
 * Created by renatopb on 06/02/16.
 */
public enum ReplyType {
    ERROR, LOGGED_IN, LOGGED_OUT, USER_INFO, OTHER_SUBSCRIBERS, SUBSCRIBED
}
