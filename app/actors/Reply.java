package actors;

import models.UserInfo;

import java.util.Collection;

/**
 * Created by renatopb on 06/02/16.
 */
public class Reply {

    private ReplyType type;
    private String text;
    private String token;
    private UserInfo userInfo;
    private Collection<UserInfo> people;

    private Reply() {}

    public static Reply userInfo(UserInfo userInfo) {
        Reply reply = new Reply();
        reply.userInfo = userInfo;
        reply.type = ReplyType.USER_INFO;
        return reply;
    }

    public static Reply otherSubscribers(Collection<UserInfo> otherSubscribers) {
        Reply reply = new Reply();
        reply.people = otherSubscribers;
        reply.type = ReplyType.OTHER_SUBSCRIBERS;
        return reply;
    }

    public static Reply subscribed(String text) {
        Reply r = new Reply();
        r.text = text;
        r.type = ReplyType.SUBSCRIBED;
        return r;
    }

    public static Reply error(String text) {
        Reply r = new Reply();
        r.text = text;
        r.type = ReplyType.ERROR;
        return r;
    }

    public static Reply login(String token) {
        Reply r = new Reply();
        r.token = token;
        r.type = ReplyType.LOGGED_IN;
        return r;
    }

    public static Reply logout() {
        Reply r = new Reply();
        r.type = ReplyType.LOGGED_OUT;
        return r;
    }

    public ReplyType getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getToken() {
        return token;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public Collection<UserInfo> getPeople() {
        return people;
    }
}
