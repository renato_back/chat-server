package actors;

import actors.messages.*;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.util.Timeout;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.Logger;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.WebSocket;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.nio.channels.ClosedChannelException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static akka.pattern.Patterns.ask;

/**
 * Created by renatopb on 05/02/16.
 */
public class Replicant extends UntypedActor {

    private static final Timeout ONE_SECOND = Timeout.apply(1, TimeUnit.SECONDS);
    private static final Reply ALREADY_SUBSCRIBED = Reply.error("User already subscribed.");

    private static ActorRef replicator = Akka.system().actorOf(Props.create(Replicant.class));

    private Map<UserInfo, WebSocket.Out<JsonNode>> subscribers = new HashMap<>();

    public static void subscribe(UserInfo userInfo, WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out)
            throws Exception {
        Reply reply = (Reply) Await.result(ask(replicator, Messages.makeSubscription(userInfo, out), ONE_SECOND),
                Duration.create(1, TimeUnit.SECONDS));

        if (ReplyType.ERROR.equals(reply.getType())) {
            out.write(error(reply));
            return;
        }

        in.onMessage(jsonNode -> {
            Logger.debug("onMessage replicator.tell");
            replicator.tell(Messages.makeText(userInfo, jsonNode.get("text").asText()), ActorRef.noSender());
        });

        in.onClose(() -> replicator.tell(Messages.makeLeave(userInfo), ActorRef.noSender()));

        out.write(Json.toJson(reply));
    }

    private static JsonNode error(Reply reply) {
        ObjectNode result = Json.newObject();
        result.put("error", reply.getText());
        return result;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ChatMessage) {
            _onReceive((ChatMessage) message);
            return;
        }

        unhandled(message);
    }

    private void _onReceive(ChatMessage message) {
        switch (message.getType()) {
            case LEAVE:
                handleLeaving((Leave) message);
                break;
            case SUBSCRIBE:
                handleSubscription((Subscription) message);
                break;
            case TEXT:
                handleText((Text) message);
                break;
            default:
                unhandled(message);
        }
    }

    private void handleText(Text message) {
        broadcast(message);
    }

    private void handleSubscription(Subscription message) {
        if (subscribers.containsKey(message.getUserInfo())) {
            getSender().tell(ALREADY_SUBSCRIBED, getSelf());
            return;
        }

        broadcast(message);

        Collection<UserInfo> filteredUsers = subscribers.keySet();
        Logger.debug("All users: " + filteredUsers.toString());
        filteredUsers = subscribers.keySet()
                .stream()
                .filter(userInfo -> !userInfo.getEmail().equals(message.getUserInfo().getEmail()))
                .collect(Collectors.toList());
        Logger.debug("Filtered users: " + filteredUsers.toString());

        getSender().tell(Reply.otherSubscribers(filteredUsers), getSelf());

        subscribers.put(message.getUserInfo(), message.getOut());

        Logger.debug("SUBSCRIBED: {userInfo: %s}", message.getUserInfo());

    }

    private void handleLeaving(Leave message) {
        WebSocket.Out<JsonNode> removed = subscribers.remove(message.getUserInfo());
        if (removed == null) {
            Logger.debug("FAILED to UNSUBSCRIBE %s.", message.getUserInfo());
            return;
        }
        Logger.debug("UNSUBSCRIBED: {userInfo: %s}", message.getUserInfo());
        broadcast(message);
    }

    private void broadcast(Message message) {
        subscribers.entrySet().parallelStream().forEach(entry -> {
            try {
                entry.getValue().write(Json.toJson(message));
            } catch (Exception e) {
                Logger.warn("Unable to send message to %s", entry.getKey());
            }
        });
    }

}
