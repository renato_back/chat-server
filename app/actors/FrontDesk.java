package actors;

import actors.messages.*;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.util.Timeout;
import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import play.Logger;
import play.libs.Akka;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import java.net.URI;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static akka.pattern.Patterns.ask;

/**
 * Created by renatopb on 06/02/16.
 */
public class FrontDesk extends UntypedActor {

    private static final Timeout TEN_SECONDS = Timeout.apply(10, TimeUnit.SECONDS);
    private static final String GOOGLE_SECRET = "KxEffWSvUcKck_fOlXK-B1nr";
    private static final String ACCESS_TOKEN_URL = "https://accounts.google.com/o/oauth2/token";
    private static final String PEOPLE_API_URL = "https://www.googleapis.com/plus/v1/people/me/openIdConnect";
    private static final String AUTH_HEADER_KEY = "Authorization";

    private static ActorRef usher = Akka.system().actorOf(Props.create(FrontDesk.class));

    private Map<String, UserInfo> people = new HashMap<>();

    private static Reply askAndWait(ActorRef destination, Message message) throws Exception {
        return (Reply) Await
                .result(
                        ask(destination, message, TEN_SECONDS),
                        TEN_SECONDS.duration());
    }

    public static Optional<Reply> login(Payload payload) {
        try {
            Reply result = askAndWait(usher, Messages.makeLogin(payload));

            if (ReplyType.ERROR.equals(result.getType())) {
                return Optional.empty();
            }

            return Optional.of(result);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static Optional<Reply> logout(String token) {
        try {
            return Optional.of(askAndWait(usher, Messages.makeLogout(token)));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static Optional<Reply> userInfo(String token) {
        try {
            Reply result =  askAndWait(usher, Messages.makeUserInfo(token));
            if (ReplyType.ERROR.equals(result.getType())) {
                return Optional.empty();
            }

            return Optional.of(result);

        } catch (Exception e) {
            return Optional.empty();
        }

    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof AuthMessage) {
            _onReceive((AuthMessage) message);
            return;
        }

        unhandled(message);
    }

    private void _onReceive(AuthMessage message) {
        switch (message.getType()) {
            case LOGOUT:
                handleLogout((Logout) message);
                break;
            case USER_INFO_REQUEST:
                handleUserInfo((UserInfoRequest) message);
                break;
            case LOGIN:
                handleLogin((Login) message);
                break;
            default:
                unhandled(message);
        }
    }

    private void handleUserInfo(UserInfoRequest message) {
        if (!people.containsKey(message.getToken())) {
            getSender().tell(Reply.error("Unknown token"), getSelf());
            return;
        }

        getSender().tell(Reply.userInfo(people.get(message.getToken())), getSelf());
    }

    private void handleLogin(Login message) {
        final String accessToken = getAccessToken(message);

        UserInfo userInfo = getUserInfo(accessToken);

        people.put(accessToken, userInfo);
        Logger.debug("LOGIN: {accessToken: '%s', userInfo: %s}", accessToken, userInfo);

        getSender().tell(Reply.login(accessToken), getSelf());
    }

    private void handleLogout(Logout message) {
        if (people.containsKey(message.getToken())) {
            UserInfo removed = people.remove(message.getToken());
            Logger.debug("LOGOUT: {accessToken: '%s', userInfo: %s}", message.getToken(), removed);
        }

        getSender().tell(Reply.logout(), getSelf());
    }


    public static final String CLIENT_ID_KEY = "client_id", REDIRECT_URI_KEY = "redirect_uri",
            CLIENT_SECRET = "client_secret", CODE_KEY = "code", GRANT_TYPE_KEY = "grant_type",
            AUTH_CODE = "authorization_code";

    private String getAccessToken(Login message) {
        return WS.url(ACCESS_TOKEN_URL)
                .setContentType("application/x-www-form-urlencoded")
                .post(buildFormData(message.getPayload()))
                .map(WSResponse::asJson)
                .get(10, TimeUnit.SECONDS)
                .get("access_token")
                .asText();
    }

    private String buildFormData(Payload payload) {
        StringBuilder result = new StringBuilder();
        result.append("1=1");
        result.append(String.format("&%s=%s", CLIENT_ID_KEY, payload.getClientId()));
        result.append(String.format("&%s=%s", REDIRECT_URI_KEY, payload.getRedirectUri()));
        result.append(String.format("&%s=%s", CODE_KEY, payload.getCode()));
        result.append(String.format("&%s=%s", CLIENT_SECRET, GOOGLE_SECRET));
        result.append(String.format("&%s=%s", GRANT_TYPE_KEY, AUTH_CODE));
        return result.toString();
    }

    private UserInfo getUserInfo(String accessToken) {
        JsonNode jsonNode = WS.url(PEOPLE_API_URL)
                .setContentType("text/plain")
                .setHeader(AUTH_HEADER_KEY, String.format("Bearer %s", accessToken))
                .get().map(WSResponse::asJson)
                .get(10, TimeUnit.SECONDS);

        return UserInfo.create(
                jsonNode.get("name").asText(),
                jsonNode.get("email").asText(),
                URI.create(jsonNode.get("picture").asText().replace("?sz=50", "?sz=35")),
                jsonNode.get("sub").asText()
        );
    }
}
