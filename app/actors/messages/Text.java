package actors.messages;

import models.UserInfo;

/**
 * Created by renatopb on 05/02/16.
 */
public class Text implements ChatMessage {

    private UserInfo userInfo;
    private String text;

    public Text(UserInfo userInfo, String text) {
        this.userInfo = userInfo;
        this.text = text;
    }

    @Override
    public UserInfo getUserInfo() {
        return userInfo;
    }

    public String getText() {
        return text;
    }

    @Override
    public MessageType getType() {
        return MessageType.TEXT;
    }
}
