package actors.messages;

import models.Payload;

/**
 * Created by renatopb on 06/02/16.
 */
public class Login implements AuthMessage {
    private Payload payload;

    public Login(Payload payload) {
        this.payload = payload;
    }

    public Payload getPayload() {
        return payload;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOGIN;
    }
}
