package actors.messages;

import models.UserInfo;

/**
 * Created by renatopb on 07/02/16.
 */
public class UserInfoRequest implements AuthMessage {
    private String token;

    public UserInfoRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public MessageType getType() {
        return MessageType.USER_INFO_REQUEST;
    }
}
