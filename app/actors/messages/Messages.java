package actors.messages;

import com.fasterxml.jackson.databind.JsonNode;
import models.Payload;
import models.UserInfo;
import play.mvc.WebSocket;

/**
 * Created by renatopb on 05/02/16.
 */
public class Messages {
    public static Text makeText(UserInfo userInfo, String text) {
        return new Text(userInfo, text);
    }

    public static Leave makeLeave(UserInfo userInfo) {
        return new Leave(userInfo);
    }

    public static Subscription makeSubscription(UserInfo userInfo, WebSocket.Out<JsonNode> out) {
        return new Subscription(userInfo, out);
    }

    public static Login makeLogin(Payload payload) {
        return new Login(payload);
    }

    public static Logout makeLogout(String token) {
        return new Logout(token);
    }

    public static UserInfoRequest makeUserInfo(String token) {
        return new UserInfoRequest(token);
    }
}
