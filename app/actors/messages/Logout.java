package actors.messages;

/**
 * Created by renatopb on 06/02/16.
 */
public class Logout implements AuthMessage {
    private String token;

    public Logout(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOGOUT;
    }
}
