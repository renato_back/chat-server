package actors.messages;

/**
 * Created by renatopb on 05/02/16.
 */
public interface Message {
    MessageType getType();
}
