package actors.messages;

/**
 * Created by renatopb on 05/02/16.
 */
public enum MessageType {
    LEAVE, SUBSCRIBE, TEXT, LOGIN, USER_INFO_REQUEST, LOGOUT
}
