package actors.messages;

import models.UserInfo;

/**
 * Created by renatopb on 05/02/16.
 */
public class Leave implements ChatMessage {
    private UserInfo userInfo;

    public Leave(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public MessageType getType() {
        return MessageType.LEAVE;
    }

    @Override
    public UserInfo getUserInfo() {
        return userInfo;
    }
}
