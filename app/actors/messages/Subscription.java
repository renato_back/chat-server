package actors.messages;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import models.UserInfo;
import play.mvc.WebSocket;

/**
 * Created by renatopb on 05/02/16.
 */
public class Subscription implements ChatMessage {

    private UserInfo userInfo;

    @JsonIgnore
    private WebSocket.Out<JsonNode> out;

    public Subscription(UserInfo userInfo, WebSocket.Out<JsonNode> out) {
        this.userInfo = userInfo;
        this.out = out;
    }

    public WebSocket.Out<JsonNode> getOut() {
        return out;
    }

    @Override
    public UserInfo getUserInfo() {
        return userInfo;
    }

    @Override
    public MessageType getType() {
        return MessageType.SUBSCRIBE;
    }
}
