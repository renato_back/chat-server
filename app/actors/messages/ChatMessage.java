package actors.messages;

import models.UserInfo;

/**
 * Created by renatopb on 05/02/16.
 */
public interface ChatMessage extends Message {
    UserInfo getUserInfo();
}
