# server

Projeto do servidor para o Chat da Pixeon.

## Build & development

Este projeto utiliza o depende do Activator da Typesafe, então é necessário instalá-lo antes. Mais detalhes na [página do Activator](https://www.typesafe.com/activator/download).

Após instalar o Activator, execute na raiz do projeto `activator clean stage`.

Ao finalizar, execute `target/universal/stage/bin/server` e o servidor estará escutando na porta 9090.