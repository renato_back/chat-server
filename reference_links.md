# Links utilizados como referência

* [Building Modern Web Applications with AngularJS and Play Framework](http://www.toptal.com/java/building-modern-web-applications-with-angularjs-and-play-framework)
* [Getting Started with Scala and Play on Heroku](https://devcenter.heroku.com/articles/getting-started-with-scala#define-a-procfile)
* [Enabling the CORS filter](https://www.playframework.com/documentation/2.4.x/CorsFilter)
* [Applying a global CSRF filter](https://www.playframework.com/documentation/2.4.x/JavaCsrf)
* [Deploying with Git](https://devcenter.heroku.com/articles/git)
* [How to reset Heroku app and re-commit everything?](http://stackoverflow.com/questions/6104258/how-to-reset-heroku-app-and-re-commit-everything)
* [play-websockets-chat-sample | GitHub](https://github.com/heroku-examples/play-websockets-chat-sample/blob/master/app/models/ChatRoom.java)
* [Play! Framework](https://www.playframework.com)
* [playframework | GitHub](https://github.com/playframework/playframework/tree/2.0.6)